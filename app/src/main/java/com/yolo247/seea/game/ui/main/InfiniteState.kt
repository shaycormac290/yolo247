package com.yolo247.seea.game.ui.main

sealed class InfiniteState {

    object StartSetuping : InfiniteState()
    object VeryBadResponse : InfiniteState()
    class VeryGoodResponse(val infinite: String) : InfiniteState()
    object DidntFindAny : InfiniteState()

}