package com.yolo247.seea.game.ui.main

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class YoloApplication : Application()
