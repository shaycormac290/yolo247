package com.yolo247.seea.game.ui.plug.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yolo247.seea.game.domain.usecase.GetSettingsUseCase
import com.yolo247.seea.game.domain.usecase.SaveSettingsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val getSettingsUseCase: GetSettingsUseCase,
    private val saveSettingsUseCase: SaveSettingsUseCase
) : ViewModel() {

    private val _settings: MutableStateFlow<List<Boolean>?> =
        MutableStateFlow(null)
    val settings: StateFlow<List<Boolean>?> = _settings

    init {
        getSettings()
    }

    private fun getSettings() {
        viewModelScope.launch {
            getSettingsUseCase.startWork().collect { url ->
                _settings.value = url.split("_").map {
                    it.toBoolean()
                }
            }
        }
    }

    fun changeSettings(id: Int, value: Boolean) {
        viewModelScope.launch {
            saveSettingsUseCase.startWork(when (id) {
                1 -> "${value}_${settings.value?.get(1)}"
                else -> "${settings.value?.get(0)}_${value}"
            })
            getSettings()
        }
    }

}
