package com.yolo247.seea.game.ui.plug.screen

import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.gestures.animateScrollBy
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Velocity
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.yolo247.seea.game.ui.plug.viewmodel.SetupNewScreenViewModel
import com.yolo247.seea.game.R
import com.yolo247.seea.game.ui.main.ProcessBack
import com.yolo247.seea.game.ui.main.ActionButton
import com.yolo247.seea.game.ui.main.ProcessText
import com.yolo247.seea.game.ui.plug.navigation.Screens
import com.yolo247.seea.game.ui.theme.MyBlue
import com.yolo247.seea.game.ui.theme.MyDark
import kotlinx.coroutines.launch

private val VerticalScrollConsumer = object : NestedScrollConnection {
    override fun onPreScroll(available: Offset, source: NestedScrollSource) = available.copy(x = 0f)
    override suspend fun onPreFling(available: Velocity) = available.copy(x = 0f)
}

fun Modifier.disabledVerticalPointerInputScroll(disabled: Boolean = true) =
    if (disabled) this.nestedScroll(VerticalScrollConsumer) else this

val items = listOf(
    R.drawable.img,
    R.drawable.img_1,
    R.drawable.img_2,
    R.drawable.img_3,
    R.drawable.img_4,
)

@Composable
fun GameScreen(
    navController: NavController,
    setupNewScreenViewModel: SetupNewScreenViewModel,
    settings: List<Boolean>?
) {
    val context = LocalContext.current
    var buttonEnabled by remember { mutableStateOf(true) }
    val coroutineScope = rememberCoroutineScope()
    val listState1 = rememberLazyListState()
    val listState2 = rememberLazyListState()
    val listState3 = rememberLazyListState()
    val listState4 = rememberLazyListState()
    var amount by remember { mutableStateOf(10000) }
    var price by remember { mutableStateOf(100) }
    ProcessBack()
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.spacedBy(32.dp, Alignment.CenterVertically),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {
            RecommendProcess(
                title = stringResource(R.string.balance),
                value = amount
            )
            RecommendProcess(
                title = stringResource(R.string.price),
                value = price
            )
        }
        Box(
            modifier = Modifier.size(350.dp),
            contentAlignment = Alignment.Center
        ) {
            Image(
                painter = painterResource(id = R.drawable.field),
                contentDescription = null,
                modifier = Modifier.fillMaxWidth(),
                contentScale = ContentScale.FillWidth
            )
            Row(
                modifier = Modifier.height(300.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                ElementsList(state = listState1)
                ElementsList(state = listState2)
                ElementsList(state = listState3)
                ElementsList(state = listState4)
            }
        }
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(24.dp, Alignment.CenterHorizontally)
        ) {
            ActionButton(
                modifier = Modifier.width(48.dp),
                id = R.drawable.minus_button,
                isSoundEnabled = settings?.get(0) ?: false
            ) {
                if (price - 10 > 0) {
                    price -= 10
                }
            }
            ActionButton(
                modifier = Modifier.width(144.dp),
                id = R.drawable.spin_button,
                enabled = buttonEnabled,
                isSoundEnabled = settings?.get(0) ?: false
            ) {
                if (price <= amount) {
                    if (buttonEnabled) {
                        amount -= price
                        coroutineScope.launch {
                            buttonEnabled = false
                            listState1.animateScrollBy(
                                value = (0..1023).random() * 108f,
                                animationSpec = tween(durationMillis = 2000)
                            )
                            listState2.animateScrollBy(
                                value = (0..1023).random() * 108f,
                                animationSpec = tween(durationMillis = 2000)
                            )
                            listState3.animateScrollBy(
                                value = (0..1023).random() * 108f,
                                animationSpec = tween(durationMillis = 2000)
                            )
                            listState4.animateScrollBy(
                                value = (0..1023).random() * 108f,
                                animationSpec = tween(durationMillis = 2000)
                            )
                            buttonEnabled = true
                            amount += (0..1000).random()
                        }
                    }
                } else Toast.makeText(
                    context,
                    "You have no money. Please, try again later",
                    Toast.LENGTH_SHORT
                ).show()
            }
            ActionButton(
                modifier = Modifier.width(48.dp),
                id = R.drawable.plus_button,
                isSoundEnabled = settings?.get(0) ?: false
            ) {
                if (price + 10 < amount) {
                    price += 10
                }
            }
        }
    }
    BackHandler {
        setupNewScreenViewModel.animateToAnotherElement(Screens.StartScreen)
        navController.navigate(Screens.LoadingScreen.route)
    }
}

@Composable
fun RecommendProcess(title: String, value: Int) {
    Column {
        ProcessText(text = title, color = MyDark, size = 16.sp)
        Spacer(modifier = Modifier.height(4.dp))
        Box(
            contentAlignment = Alignment.Center
        ) {
            Image(
                painter = painterResource(id = R.drawable.disabled_cell),
                contentDescription = null,
                modifier = Modifier.width(108.dp),
                contentScale = ContentScale.FillWidth
            )
            ProcessText(text = value.toString(), color = MyBlue, size = 20.sp)
        }
    }
}

@Composable
fun ElementsList(state: LazyListState) {
    LazyColumn(
        state = state,
        modifier = Modifier.disabledVerticalPointerInputScroll()
    ) {
        items(1024) {
            Image(
                painter = painterResource(id = items[(items.indices).random()]),
                contentDescription = null,
                modifier = Modifier.size(68.dp)
            )
            Spacer(modifier = Modifier.padding(6.dp))
        }
    }
}
