package com.yolo247.seea.game.ui.base

import android.Manifest
import android.net.Uri
import android.os.Message
import android.webkit.CookieManager
import android.webkit.PermissionRequest
import android.webkit.ValueCallback
import android.webkit.WebView
import androidx.activity.compose.BackHandler
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState
import com.google.accompanist.web.AccompanistWebChromeClient
import com.google.accompanist.web.WebView
import com.google.accompanist.web.rememberWebViewState
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun InfiniteScreen(
    request: String,
) {
    var isPreloading by remember { mutableStateOf(true) }
    var isFinished by remember { mutableStateOf(false) }
    val scope = rememberCoroutineScope()
    val notificationPermissionState = rememberPermissionState(
        permission = Manifest.permission.CAMERA,
        onPermissionResult = {
            isFinished = true
        })
    var fileChooserValueCallback by remember { mutableStateOf<ValueCallback<Array<Uri>>?>(null) }
    val pickPictureLauncher = rememberLauncherForActivityResult(
        ActivityResultContracts.GetContent()
    ) { imageUri ->
        if (imageUri != null) {
            fileChooserValueCallback?.onReceiveValue(arrayOf(imageUri))
        }
    }
    val backEnabled = remember { mutableStateOf(false) }
    val state = rememberWebViewState(url = request)
    val chromeClient = remember {
        object : AccompanistWebChromeClient() {
            override fun onShowFileChooser(
                webView: WebView?,
                filePathCallback: ValueCallback<Array<Uri>>,
                fileChooserParams: FileChooserParams
            ): Boolean {
                pickPictureLauncher.launch("image/*")
                fileChooserValueCallback = filePathCallback
                super.onShowFileChooser(
                    webView,
                    fileChooserValueCallback,
                    fileChooserParams
                )
                return true
            }

            override fun onPermissionRequest(request: PermissionRequest) {
                notificationPermissionState.launchPermissionRequest()
                scope.launch {
                    if (!isFinished) {
                        return@launch
                    }
                    request.grant(request.resources)
                    isFinished = false
                }
            }

            override fun onCreateWindow(
                view: WebView?,
                isDialog: Boolean,
                isUserGesture: Boolean,
                resultMsg: Message?
            ): Boolean {
                isPreloading = false
                return super.onCreateWindow(view, isDialog, isUserGesture, resultMsg)
            }

        }
    }
    WebView(
        state = state,
        modifier = Modifier.fillMaxSize(),
        chromeClient = chromeClient,
        onCreated = {
            with(it.settings) {
                javaScriptEnabled = true
                loadWithOverviewMode = true
                useWideViewPort = true
                domStorageEnabled = true
                databaseEnabled = true
                setSupportZoom(false)
                allowFileAccess = true
                allowContentAccess = true
                loadWithOverviewMode = true
                useWideViewPort = true
                mediaPlaybackRequiresUserGesture = true
                loadWithOverviewMode = true
                allowFileAccessFromFileURLs = true
                allowUniversalAccessFromFileURLs = true
                val cookieManager = CookieManager.getInstance()
                cookieManager.setAcceptCookie(true)
            }
        },
    )
    if (isPreloading) {
        ProgressBar()
    }
    LaunchedEffect(key1 = true) {
        delay(2000)
        isPreloading = false
    }
    BackHandler {}
}