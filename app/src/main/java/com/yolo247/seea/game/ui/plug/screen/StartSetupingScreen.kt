package com.yolo247.seea.game.ui.plug.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.yolo247.seea.game.ui.plug.viewmodel.SetupNewScreenViewModel
import com.yolo247.seea.game.R
import com.yolo247.seea.game.ui.main.ProcessBack
import kotlinx.coroutines.delay

@Composable
fun StartSetupingScreen(
    navController: NavController,
    setupNewScreenViewModel: SetupNewScreenViewModel
) {
    val nextScreen by setupNewScreenViewModel.nextScreen.collectAsState()
    var width by remember { mutableStateOf(400) }
    ProcessBack()
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.thrash),
            contentDescription = null,
            modifier = Modifier.fillMaxWidth(.7f),
            contentScale = ContentScale.FillWidth
        )
        Image(
            painter = painterResource(id = R.drawable.loading_text),
            contentDescription = null,
            modifier = Modifier.fillMaxWidth(.7f),
            contentScale = ContentScale.FillWidth
        )
        Spacer(
            modifier = Modifier.height(24.dp)
        )
        Box(
            modifier = Modifier.fillMaxWidth(.7f).height(64.dp)
        ) {
            Image(
                painter = painterResource(id = R.drawable.loading_bar),
                contentDescription = null,
                modifier = Modifier.fillMaxSize(),
                contentScale = ContentScale.FillBounds
            )
            Spacer(
                modifier = Modifier
                    .width(width.dp)
                    .wrapContentHeight()
                    .padding(4.dp)
                    .background(Color.Black)
                    .alpha(.5f)
                    .align(Alignment.CenterEnd)
            )
        }
    }
    LaunchedEffect(key1 = true) {
        delay((1000..4000).random().toLong())
        width = 0
        delay(100)
        navController.run {
            popBackStack()
            navigate(nextScreen.route)
        }
    }
    LaunchedEffect(key1 = true) {
        while (width != 0) {
            delay(180)
            width -= (5..10).random()
        }
    }
}
