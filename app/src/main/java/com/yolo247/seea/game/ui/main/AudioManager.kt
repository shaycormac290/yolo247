package com.yolo247.seea.game.ui.main

import android.content.Context
import android.media.MediaPlayer
import com.yolo247.seea.game.R

object AudioManager {

    private var mediaPlayer: MediaPlayer? = null

    fun init(context: Context) {
        mediaPlayer = MediaPlayer.create(context.applicationContext, R.raw.sound)
    }

    fun startProcessing() {
        mediaPlayer?.start()
    }
}
