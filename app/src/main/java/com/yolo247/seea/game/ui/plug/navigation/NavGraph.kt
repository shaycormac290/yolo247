package com.yolo247.seea.game.ui.plug.navigation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.background
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.composable
import com.yolo247.seea.game.ui.plug.viewmodel.SetupNewScreenViewModel
import com.yolo247.seea.game.ui.plug.screen.GameScreen
import com.yolo247.seea.game.ui.plug.screen.StartSetupingScreen
import com.yolo247.seea.game.ui.plug.screen.OptionScreen
import com.yolo247.seea.game.ui.plug.screen.StartScreen
import com.yolo247.seea.game.ui.plug.viewmodel.SettingsViewModel

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun SetupNavGraph(
    navController: NavHostController,
    settingsViewModel: SettingsViewModel,
) {
    val settings by settingsViewModel.settings.collectAsState()
    val setupNewScreenViewModel = SetupNewScreenViewModel()
    AnimatedNavHost(
        modifier = Modifier.background(MaterialTheme.colorScheme.background),
        navController = navController,
        startDestination = Screens.LoadingScreen.route,
        builder = {
            navScreen(
                route = Screens.LoadingScreen.route,
                screen = {
                    StartSetupingScreen(
                        navController = navController,
                        setupNewScreenViewModel = setupNewScreenViewModel
                    )
                }
            )
            navScreen(
                route = Screens.StartScreen.route,
                screen = {
                    StartScreen(
                        navController = navController,
                        setupNewScreenViewModel = setupNewScreenViewModel,
                        settings = settings
                    )
                }
            )
            navScreen(
                route = Screens.GameScreen.route,
                screen = {
                    GameScreen(
                        navController = navController,
                        setupNewScreenViewModel = setupNewScreenViewModel,
                        settings = settings
                    )
                }
            )
            navScreen(
                route = Screens.OptionScreen.route,
                screen = {
                    OptionScreen(
                        navController = navController,
                        settingsViewModel = settingsViewModel,
                    )
                }
            )
        }
    )
}

@OptIn(ExperimentalAnimationApi::class)
fun NavGraphBuilder.navScreen(route: String, screen: @Composable () -> Unit) {
    composable(
        route = route,
        exitTransition = {
            slideOutVertically(
                targetOffsetY = { -400 },
                animationSpec = tween(
                    durationMillis = 300,
                    easing = FastOutSlowInEasing
                )
            ) + fadeOut(animationSpec = tween(300))
        },
        popEnterTransition = {
            slideInVertically(
                initialOffsetY = { 400 },
                animationSpec = tween(
                    durationMillis = 300,
                    easing = FastOutSlowInEasing
                )
            ) + fadeIn(animationSpec = tween(300))
        },
    ) {
        screen()
    }
}
