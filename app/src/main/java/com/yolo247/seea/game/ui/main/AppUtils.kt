package com.yolo247.seea.game.ui.main

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp
import com.yolo247.seea.game.R

@Composable
fun ProcessBack() {
    Image(
        painter = painterResource(id = R.drawable.my_back),
        contentDescription = null,
        modifier = Modifier.fillMaxSize(),
        contentScale = ContentScale.Crop
    )
}

@Composable
fun ProcessText(
    modifier: Modifier? = Modifier,
    text: String,
    size: TextUnit? = null,
    color: Color? = null,
    textAlign: TextAlign = TextAlign.Center
) {
    Text(
        modifier = modifier ?: Modifier,
        color = color ?: Color.White,
        text = text,
        fontFamily = FontFamily(Font(R.font.slack)),
        fontSize = size ?: 16.sp,
        overflow = TextOverflow.Ellipsis,
        maxLines = 7,
        textAlign = textAlign
    )
}

@Composable
fun ActionButton(
    modifier: Modifier = Modifier.fillMaxWidth(.75f),
    id: Int,
    enabled: Boolean = true,
    isSoundEnabled: Boolean,
    onClick: () -> Unit
) {
    Image(
        painter = painterResource(id = id),
        contentDescription = null,
        modifier = modifier
            .clip(RoundedCornerShape(20))
            .clickable {
                if (isSoundEnabled) AudioManager.startProcessing()
                onClick.invoke()
            },
        contentScale = ContentScale.FillWidth
    )
}
