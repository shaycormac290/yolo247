package com.yolo247.seea.game.ui.plug.navigation

sealed class Screens(val route: String) {

    object StartScreen: Screens("start_screen")
    object LoadingScreen: Screens("loading_screen")
    object OptionScreen: Screens("option_screen")
    object GameScreen: Screens("game_screen")

}
