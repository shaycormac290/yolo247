package com.yolo247.seea.game.di

import android.app.Application
import android.content.Context
import com.yolo247.seea.game.data.repository.SettingsRepositoryImpl
import com.yolo247.seea.game.domain.repository.SettingsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepoModule {

    @Provides
    @Named("Settings")
    @Singleton
    fun provideSettingsRepository(
        @ApplicationContext context: Context
    ): SettingsRepository = SettingsRepositoryImpl(context)

    @Provides
    @Singleton
    fun provideContext(
        app: Application
    ): Context = app.applicationContext

}
