package com.yolo247.seea.game.domain.usecase

import com.yolo247.seea.game.domain.repository.SettingsRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class SaveSettingsUseCase @Inject constructor(
    @Named("Settings") private val repository: SettingsRepository,
    @Named("io") private val dispatcher: CoroutineDispatcher
) {

    suspend fun startWork(url: String) =
        withContext(dispatcher) {
            repository.saveUrl(url)
        }

}
