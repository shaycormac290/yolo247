package com.yolo247.seea.game.ui.plug.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.yolo247.seea.game.R
import com.yolo247.seea.game.ui.main.MainActivity
import com.yolo247.seea.game.ui.main.ProcessBack
import com.yolo247.seea.game.ui.main.ActionButton
import com.yolo247.seea.game.ui.main.ProcessText
import com.yolo247.seea.game.ui.plug.viewmodel.SettingsViewModel
import com.yolo247.seea.game.ui.theme.MyBlue

@Composable
fun OptionScreen(
    navController: NavController,
    settingsViewModel: SettingsViewModel,
) {
    val activity = LocalContext.current as MainActivity
    val settings by settingsViewModel.settings.collectAsState()
    ProcessBack()
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(64.dp, Alignment.CenterVertically)
    ) {
        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(32.dp, Alignment.CenterVertically)
        ) {
            Image(
                painter = painterResource(id = R.drawable.options_text),
                contentDescription = null,
                modifier = Modifier.fillMaxWidth(.6f),
                contentScale = ContentScale.FillWidth
            )
            Setting(
                s = "SOUND",
                currentValue = settings?.get(0) ?: false,
            ) {
                settingsViewModel.changeSettings(1, it)
            }
            Setting(
                s = "MUSIC",
                currentValue = settings?.get(1) ?: false,
            ) {
                if (it) {
                    activity.startPlayer()
                } else {
                    activity.stopPlayer()
                }
                settingsViewModel.changeSettings(2, it)
            }
        }
        ActionButton(id = R.drawable.back_button, isSoundEnabled = settings?.get(0) ?: false) {
            navController.popBackStack()
        }
    }
}

@Composable
fun Setting(
    s: String,
    currentValue: Boolean,
    function: (Boolean) -> Unit
) {
    Box(
        modifier = Modifier
            .fillMaxWidth(.8f)
            .clip(RoundedCornerShape(10))
            .clickable {
                function.invoke(!currentValue)
            },
        contentAlignment = Alignment.Center
    ) {
        Image(
            painter = painterResource(id = if (!currentValue) R.drawable.disabled_cell else R.drawable.enabled_cell),
            contentDescription = null,
            modifier = Modifier.fillMaxWidth(),
            contentScale = ContentScale.FillWidth
        )
        ProcessText(
            text = "$s: ${if (!currentValue) "OFF" else "ON"}",
            size = 28.sp,
            color = MyBlue
        )
    }
}
