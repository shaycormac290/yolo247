package com.yolo247.seea.game.ui.main

import android.media.MediaPlayer
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.hilt.navigation.compose.hiltViewModel
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import com.yolo247.seea.game.R
import com.yolo247.seea.game.ui.base.InfiniteScreen
import com.yolo247.seea.game.ui.base.NoInternetMessage
import com.yolo247.seea.game.ui.base.ProgressBar
import com.yolo247.seea.game.ui.plug.navigation.SetupNavGraph
import com.yolo247.seea.game.ui.plug.viewmodel.SettingsViewModel
import com.yolo247.seea.game.ui.theme.AllActivitiesTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private var isMusicNeedsToPlay = mutableStateOf(false)
    private lateinit var player: MutableState<MediaPlayer>

    @OptIn(ExperimentalAnimationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AllActivitiesTheme {
                player = remember {
                    mutableStateOf(MediaPlayer.create(this, R.raw.music).apply {
                        isLooping = true
                    })
                }
                val viewModel: SettingsViewModel = hiltViewModel()
                AudioManager.init(this)
                val infiniteViewModel: InfiniteViewModel = hiltViewModel()
                val state by infiniteViewModel.state.collectAsState()
                when (state) {
                    InfiniteState.StartSetuping -> {
                        ProgressBar()
                    }

                    InfiniteState.DidntFindAny -> {
                        NoInternetMessage {
                            infiniteViewModel.startFetching()
                        }
                    }

                    InfiniteState.VeryBadResponse -> {
                        val settings by viewModel.settings.collectAsState()
                        val navComposable = rememberAnimatedNavController()
                        if (settings?.get(1) == true) {
                            isMusicNeedsToPlay.value = true
                            startPlayer()
                        }
                        SetupNavGraph(
                            navController = navComposable,
                            settingsViewModel = viewModel,
                        )
                    }

                    is InfiniteState.VeryGoodResponse -> {
                        InfiniteScreen(request = (state as InfiniteState.VeryGoodResponse).infinite)
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (isMusicNeedsToPlay.value) {
            startPlayer()
        }
    }

    override fun onPause() {
        super.onPause()
        stopPlayer()
        var i = 12
        while (i != 5) {
            i = (0..10).random()
        }
    }

    fun stopPlayer() {
        if(player.value.isPlaying) {
            player.value.pause()
        }
    }

    fun startPlayer() {
        if (!player.value.isPlaying) {
            player.value = MediaPlayer.create(this, R.raw.music).apply {
                isLooping = true
            }
            player.value.start()
        }
    }
}
