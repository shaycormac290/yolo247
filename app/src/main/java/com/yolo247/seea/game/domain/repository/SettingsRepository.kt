package com.yolo247.seea.game.domain.repository

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import kotlinx.coroutines.flow.Flow

interface SettingsRepository {

    val onBoardingDataStore: DataStore<Preferences>
    suspend fun saveUrl(url: String)
    fun getLastUrl(): Flow<String>

}
