package com.yolo247.seea.game.ui.plug.screen

import android.app.Activity
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.yolo247.seea.game.ui.plug.viewmodel.SetupNewScreenViewModel
import com.yolo247.seea.game.R
import com.yolo247.seea.game.ui.main.ProcessBack
import com.yolo247.seea.game.ui.main.ActionButton
import com.yolo247.seea.game.ui.plug.navigation.Screens

@Composable
fun StartScreen(
    navController: NavController,
    settings: List<Boolean>?,
    setupNewScreenViewModel: SetupNewScreenViewModel,
) {
    val context = LocalContext.current as Activity
    ProcessBack()
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(32.dp, Alignment.CenterVertically)
    ) {
        ActionButton(id = R.drawable.play_button, isSoundEnabled = settings?.get(0) ?: false) {
            setupNewScreenViewModel.animateToAnotherElement(Screens.GameScreen)
            navController.navigate(Screens.LoadingScreen.route)
        }
        ActionButton(id = R.drawable.options_button, isSoundEnabled = settings?.get(0) ?: false) {
            navController.navigate(Screens.OptionScreen.route)
        }
        ActionButton(id = R.drawable.exit_button, isSoundEnabled = settings?.get(0) ?: false) {
            context.finish()
        }
    }
}
