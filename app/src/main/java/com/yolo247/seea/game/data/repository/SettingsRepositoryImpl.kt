package com.yolo247.seea.game.data.repository

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.yolo247.seea.game.domain.repository.SettingsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException
import javax.inject.Inject

val Context.settingsStore: DataStore<Preferences> by preferencesDataStore(name = "settings_pref")

class SettingsRepositoryImpl @Inject constructor(
    context: Context
) : SettingsRepository {

    private object PreferenceKey {
        val onBoardingKey = stringPreferencesKey(name = "settings_pref")
    }

    override val onBoardingDataStore = context.settingsStore

    override suspend fun saveUrl(url: String) {
        onBoardingDataStore.edit {
            it[PreferenceKey.onBoardingKey] = url
        }
    }

    override fun getLastUrl(): Flow<String> {
        return onBoardingDataStore.data
            .catch { exception ->
                if (exception is IOException) {
                    emit(emptyPreferences())
                } else throw exception
            }
            .map { preferences ->
                preferences[PreferenceKey.onBoardingKey] ?: "false_false"
            }
    }

}
