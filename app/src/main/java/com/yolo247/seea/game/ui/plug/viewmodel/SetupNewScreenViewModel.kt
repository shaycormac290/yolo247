package com.yolo247.seea.game.ui.plug.viewmodel

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import com.yolo247.seea.game.ui.plug.navigation.Screens

class SetupNewScreenViewModel : ViewModel() {

    private val _nextScreen: MutableStateFlow<Screens> =
        MutableStateFlow(Screens.StartScreen)
    val nextScreen: StateFlow<Screens> = _nextScreen

    fun animateToAnotherElement(screens: Screens) {
        var index = 0
        _nextScreen.value = screens
        while (index < 10) {
            ++index
        }
    }
}
