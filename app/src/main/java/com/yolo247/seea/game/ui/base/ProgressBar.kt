package com.yolo247.seea.game.ui.base

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.yolo247.seea.game.R

@Composable
fun ProgressBar() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black)
            .padding(40.dp)
    ) {
        Image(
            painter = painterResource(id = R.drawable.favicon),
            contentDescription = null,
            modifier = Modifier
                .align(Alignment.Center)
                .size(300.dp)
                .clip(CircleShape),
            contentScale = ContentScale.Crop
        )
        CircularProgressIndicator(color = Color.White, modifier = Modifier.align(Alignment.BottomCenter).size(40.dp))
    }
}